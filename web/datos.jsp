<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Formulario de Datos</h1>
        <form action="resultados.jsp" method="post">
            <%-- a donde envio el formulario --%>
            Producto: <select name="selectPro">
                <option value="Televisor">Televisor</option>
                <%--    Value es el valor que se envia , el otro es el que se muestra por pantalla     --%>
                <option value="Estereo">Estereo</option>
                <option value="Lavarropas">Lavarropas</option>
            </select><br>
            Precio: <input type="text" name="textPre"><br>
            Cantidad: <input type="text" name="textCant"><br>
            <input type="submit" name="btn1" value="Enviar">
        </form>
    </body>
</html>
