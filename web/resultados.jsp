<%-- 
    Document   : resultados
    Created on : Sep 23, 2019, 12:49:32 PM
    Author     : mnarvaja
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Resultados</h1>
        
        <%
            String pro = request.getParameter("selectPro");
            //selectPro es el nombre que le dimos a la etiqueta donde esta el producto en datos.jsp
            double precio = Double.parseDouble(request.getParameter("textPre")); 
            //Hay que convertirlo a Double ya que request.getParameter lo recibe como texto
            int cantidad  = Integer.parseInt(request.getParameter("textCant"));
            
            double subtotal = cantidad * precio;
            double iva = subtotal*0.21;
            double total = subtotal + iva;
        %>
        Producto: <%=pro%><br>
        Precio: <%=precio%><br>
        Cantidad: <%=cantidad%><br>
        Subtotal: <%=subtotal%><br>
        IVA 21%: <%=iva%><br>
        Total: <%=total%><br>
        
        <a href="datos.jsp">
            Volver a Datos
        </a>
    </body>
</html>
