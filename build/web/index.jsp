<%-- 
    Document   : index
    Created on : Sep 23, 2019, 12:07:02 PM
    Author     : mnarvaja
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Tipos de dato</h1>
        
        <%
            String nom = "Jose";
            int edad  = 25;
            double sueldo = 4000.56;
            
            out.println("Nombre : "+ nom+"<br>");
            out.println("Edad : "+ edad+"<br>");
            out.println("Sueldo : "+ sueldo);
        %>
    </body>
</html>
